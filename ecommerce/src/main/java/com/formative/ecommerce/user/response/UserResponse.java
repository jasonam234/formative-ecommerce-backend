package com.formative.ecommerce.user.response;

import java.util.List;

import com.formative.ecommerce.user.entity.User;

public class UserResponse implements Response{
	int status;
	String message;
	List<User> data;
	
	public UserResponse(int status, String message, List<User> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UserResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
