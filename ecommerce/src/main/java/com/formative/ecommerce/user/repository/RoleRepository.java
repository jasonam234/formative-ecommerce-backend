package com.formative.ecommerce.user.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.formative.ecommerce.user.entity.Role;


@Transactional
public interface RoleRepository extends JpaRepository<Role, Integer>{

}
