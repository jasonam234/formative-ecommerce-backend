package com.formative.ecommerce.user.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detail_user")
public class DetailUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "detail_user_id")
	int detail_user_id;
	String fullname;
	String address;
	String phone_number;
	String created_at;
	
	@OneToOne(mappedBy = "detailUser")
	User user;
	
	protected DetailUser() {}
	
	public DetailUser(int detail_user_id, String fullname, String address, String phone_number, String created_at,
			User user) {
		super();
		this.detail_user_id = detail_user_id;
		this.fullname = fullname;
		this.address = address;
		this.phone_number = phone_number;
		this.created_at = created_at;
		this.user = user;
	}

	public int getDetail_user_id() {
		return detail_user_id;
	}

	public void setDetail_user_id(int detail_user_id) {
		this.detail_user_id = detail_user_id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "DetailUser [detail_user_id=" + detail_user_id + ", fullname=" + fullname + ", address=" + address
				+ ", phone_number=" + phone_number + ", created_at=" + created_at + "]";
	}
}
