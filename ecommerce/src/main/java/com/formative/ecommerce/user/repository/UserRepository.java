package com.formative.ecommerce.user.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.formative.ecommerce.user.entity.DetailUser;
import com.formative.ecommerce.user.entity.Role;
import com.formative.ecommerce.user.entity.User;

@Transactional
public interface UserRepository extends JpaRepository<User, Integer>{
	@Query(value = "SELECT * FROM user", nativeQuery = true)
	List<User> getAllUser();
	
	@Query(value = "SELECT * FROM user WHERE user_id = ?1", nativeQuery = true)
	User getUserById(int user_id);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO user(username, password, role, detail_user, created_at) "
			+ "VALUE (?1, ?2, 1, ?3, now());",
			nativeQuery = true)
	int insertUser(String username, String password, int detail_user);

	@Query(value = "SELECT * FROM user WHERE user_id=(SELECT LAST_INSERT_ID())", nativeQuery = true)
	User getLastUser();
	
	@Query(value = "SELECT * FROM user WHERE username = ?1 AND password = ?2",
			nativeQuery = true)
	User loginUser(String username, String password);
}
