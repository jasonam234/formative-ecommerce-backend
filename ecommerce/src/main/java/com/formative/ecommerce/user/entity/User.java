package com.formative.ecommerce.user.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.formative.ecommerce.store.entity.Store;
import com.formative.ecommerce.transaction.entity.Transaction;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	int user_id;
	String username;
	String password;
	String created_at;
	
	@ManyToOne
	@JoinColumn(name = "role", referencedColumnName="role_id")
	Role role;
	
	@OneToOne
	@JoinColumn(name = "detail_user", referencedColumnName="detail_user_id")
	DetailUser detailUser;
	
	@OneToOne(mappedBy = "user")
	Store store;

	@OneToMany(mappedBy = "user")
	List<Transaction> transaction = new ArrayList<>();
	
	protected User() {}

	public User(int user_id, String username, String password, String created_at, Role role, DetailUser detailUser) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.created_at = created_at;
		this.role = role;
		this.detailUser = detailUser;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public DetailUser getDetailUser() {
		return detailUser;
	}

	public void setDetailUser(DetailUser detailUser) {
		this.detailUser = detailUser;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", username=" + username + ", password=" + password + ", created_at="
				+ created_at + ", role=" + role + ", detailUser=" + detailUser + "]";
	}
}
