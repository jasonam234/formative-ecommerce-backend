package com.formative.ecommerce.user.response;

public class UserResponseError implements Response{
	int status;
	String message;
	
	public UserResponseError(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "UserResponseError [status=" + status + ", message=" + message + "]";
	}
}
