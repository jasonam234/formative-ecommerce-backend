package com.formative.ecommerce.user.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.formative.ecommerce.user.entity.DetailUser;

@Transactional
public interface DetailUserRepository extends JpaRepository<DetailUser, Integer>{
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO detail_user(fullname, address, phone_number, created_at) VALUE(?1, ?2, ?3, now());",
			nativeQuery = true)
	int insertDetailUser(String fullName, String address, String phone_number);
	
	@Query(value = "SELECT * FROM detail_user WHERE detail_user_id=(SELECT LAST_INSERT_ID())", nativeQuery = true)
	DetailUser getLastDetailUser();
}
