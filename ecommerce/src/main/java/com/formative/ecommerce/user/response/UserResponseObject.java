package com.formative.ecommerce.user.response;

import com.formative.ecommerce.user.entity.User;

public class UserResponseObject implements Response{
	int status;
	String message;
	User data;
	
	public UserResponseObject(int status, String message, User data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getData() {
		return data;
	}

	public void setData(User data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UserResponseObject [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
