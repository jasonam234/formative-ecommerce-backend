package com.formative.ecommerce.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.formative.ecommerce.store.entity.Product;
import com.formative.ecommerce.store.entity.Store;
import com.formative.ecommerce.store.repository.ProductRepository;
import com.formative.ecommerce.store.repository.StoreRepository;
import com.formative.ecommerce.store.response.ProductResponse;
import com.formative.ecommerce.store.response.ProductResponseObject;
import com.formative.ecommerce.store.response.StoreResponse;
import com.formative.ecommerce.store.response.StoreResponseObject;
import com.formative.ecommerce.transaction.entity.Transaction;
import com.formative.ecommerce.transaction.entity.TransactionDetail;
import com.formative.ecommerce.transaction.repository.TransactionDetailRepository;
import com.formative.ecommerce.transaction.repository.TransactionRepository;
import com.formative.ecommerce.transaction.response.TransactionDetailResponse;
import com.formative.ecommerce.transaction.response.TransactionResponse;
import com.formative.ecommerce.user.entity.DetailUser;
import com.formative.ecommerce.user.entity.Role;
import com.formative.ecommerce.user.entity.User;
import com.formative.ecommerce.user.repository.DetailUserRepository;
import com.formative.ecommerce.user.repository.RoleRepository;
import com.formative.ecommerce.user.repository.UserRepository;
import com.formative.ecommerce.user.response.Response;
import com.formative.ecommerce.user.response.UserResponse;
import com.formative.ecommerce.user.response.UserResponseError;
import com.formative.ecommerce.user.response.UserResponseObject;

@RestController
public class UserController {
	@Autowired
	UserRepository userRepository;

	@Autowired
	DetailUserRepository detailUserRepository;

	@Autowired
	RoleRepository roleRepository;

	@GetMapping(value = "/api/users", produces = "application/json")
	public ResponseEntity<Response> getAllUser() {
		List<User> data = userRepository.getAllUser();
		Response response = new UserResponse(HttpStatus.OK.value(), "GET all user operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping(value = "/api/user/{id}", produces = "application/json")
	public ResponseEntity<Response> getUserById(@PathVariable int id) {
		User result = userRepository.getUserById(id);
		Response response = new UserResponseObject(HttpStatus.OK.value(), "GET user operation succesful", result);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping(value = "/api/user", produces = "application/json")
	public ResponseEntity<Response> postUser(@RequestBody User user) {
		DetailUser du = user.getDetailUser();
		List<User> data = userRepository.getAllUser();
		boolean checkPassword = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
				.matcher(user.getPassword()).find();
		boolean checkPhoneNumber = Pattern.compile("[0-9]{10,}").matcher(user.getDetailUser().getPhone_number()).find();
		boolean checkUsername = Pattern.compile("^(.+)@(.+)$").matcher(user.getUsername()).find();
		boolean invalidUsername = false;
		for (User u : data) {
			if (u.getUsername().equalsIgnoreCase(user.getUsername())) {
				invalidUsername = true;
			}
		}
		System.out.println(invalidUsername + " : " + user.getUsername() + " : " + checkUsername);
		if (checkPassword && checkPhoneNumber && !invalidUsername && checkUsername) {
			List<User> result = new ArrayList<>();
			detailUserRepository.insertDetailUser(du.getFullname(), du.getAddress(), du.getPhone_number());
			DetailUser detailUser = detailUserRepository.getLastDetailUser();

			userRepository.insertUser(user.getUsername(), user.getPassword(), detailUser.getDetail_user_id());
			result.add(userRepository.getLastUser());
			Response response = new UserResponse(HttpStatus.OK.value(), "POST user process successful", result);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		Response response = new UserResponseError(HttpStatus.UNAUTHORIZED.value(), 
				"POST user process terminated, invalid username or password");
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
	}

	//Change user role
	@PutMapping(value = "/api/user/{id}", produces = "application/json")
	public ResponseEntity<Response> updateUserToStore(@PathVariable int id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isEmpty()) {
			Response response = new UserResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"PUT user role process terminated, user not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		user.get().setRole(roleRepository.findById(2).get());
		userRepository.save(user.get());
		List<User> result = new ArrayList<>();
		result.add(user.get());
		UserResponse response = new UserResponse(HttpStatus.OK.value(), "PUT user role succesful", result);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	//login
	@PostMapping("/api/auth/login")
	public ResponseEntity<Response> loginUser(@RequestBody User user) {
		User login = userRepository.loginUser(user.getUsername(), user.getPassword());
		if(login == null) {
			Response response = new UserResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"GET user login process terminated, username not found");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		List<User> result = new ArrayList<>();
		result.add(login);
		Response response = new UserResponse(HttpStatus.OK.value(), 
				"GET user login process successful",result);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
