package com.formative.ecommerce.transaction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.formative.ecommerce.store.entity.Store;
import com.formative.ecommerce.transaction.entity.Transaction;
import com.formative.ecommerce.user.entity.User;

@Transactional
public interface TransactionRepository extends JpaRepository<Transaction, Integer>{
	@Query(value = "SELECT * FROM transaction", nativeQuery = true)
	List<Transaction> getAllTransaction();
	
	@Query(value = "SELECT * FROM transaction WHERE transaction_id = ?1", nativeQuery = true)
	Transaction getTransactionById(int transaction_id);
	
	@Query(value = "SELECT * FROM transaction WHERE user_id = ?1 ORDER BY transaction_id DESC LIMIT 0, 1", nativeQuery = true)
	List<Transaction> getTransactionByUserId(int user_id);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO transaction(user_id, grand_total, status, created_at) "
			+ "VALUE(?1, ?2, false, now());"
			,nativeQuery = true)
	int transactionCheckout(int user_id, long grand_total);
}
