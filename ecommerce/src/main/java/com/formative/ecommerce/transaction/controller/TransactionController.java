package com.formative.ecommerce.transaction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.formative.ecommerce.transaction.entity.Transaction;
import com.formative.ecommerce.transaction.entity.TransactionDetail;
import com.formative.ecommerce.transaction.repository.TransactionDetailRepository;
import com.formative.ecommerce.transaction.repository.TransactionRepository;
import com.formative.ecommerce.transaction.response.Response;
import com.formative.ecommerce.transaction.response.TransactionDetailResponse;
import com.formative.ecommerce.transaction.response.TransactionResponse;
import com.formative.ecommerce.transaction.response.TransactionResponseObject;

@RestController
public class TransactionController {
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	TransactionDetailRepository transactionDetailRepository;
	
	@GetMapping(value = "/api/transactions", produces = "application/json")
	public ResponseEntity<Response> getAllTransaction() {
		List<Transaction> data = transactionRepository.getAllTransaction();
		Response response = new TransactionResponse(HttpStatus.OK.value(), "GET all transactions operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/api/transaction/{id}", produces = "application/json")
	public ResponseEntity<Response> getTransactionById(@PathVariable int id) {
		Transaction result = transactionRepository.getTransactionById(id);
		Response response = new TransactionResponseObject(HttpStatus.OK.value(), "GET transaction by ID succesful", result);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/api/transaction_detail", produces = "application/json")
	public ResponseEntity<Response> getAllTransactionDetail() {
		List<TransactionDetail> data = transactionDetailRepository.getAllTransactionDetail();
		Response response = new TransactionDetailResponse(HttpStatus.OK.value(), "GET all transactions operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value ="/api/transaction/checkout", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> userTransactionCheckout(@RequestBody Transaction transaction){
		transactionRepository.transactionCheckout(transaction.getUser().getUser_id(), transaction.getGrand_total());
		List<Transaction> data = transactionRepository.getTransactionByUserId(transaction.getUser().getUser_id());
		Response  response = new TransactionResponse(HttpStatus.OK.value(), "POST transaction operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "/api/detail_transaction/checkout", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> userTransactionDetailCheckout(@RequestBody List<TransactionDetail> transactionDetail){
		for(int i = 0; i < transactionDetail.size(); i++) {
			transactionDetailRepository.transactionDetailCheckout(transactionDetail.get(i).getTransaction().getTransaction_id(), 
					transactionDetail.get(i).getProduct().getProduct_id(), 
					transactionDetail.get(i).getQty_product(), 
					transactionDetail.get(i).getTotal());
		}
		List<TransactionDetail> data = transactionDetailRepository.getLastInsertedTransactionDetail(transactionDetail.size());
		Response response = new TransactionDetailResponse(HttpStatus.OK.value(), "POST transaction detail operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
