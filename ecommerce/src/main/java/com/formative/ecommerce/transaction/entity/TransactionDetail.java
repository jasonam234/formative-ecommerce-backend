package com.formative.ecommerce.transaction.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.formative.ecommerce.store.entity.Product;

@Entity
@Table(name = "transaction_detail")
public class TransactionDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int transaction_detail_id;
	int qty_product;
	long total;
	boolean status;
	String created_at;
	
	@ManyToOne
	@JoinColumn(name = "transaction_id", referencedColumnName="transaction_id")
	Transaction transaction;	
	
	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName="product_id")
	Product product;

	public TransactionDetail(int transaction_detail_id, int qty_product, long total, boolean status, String created_at,
			Transaction transaction, Product product) {
		super();
		this.transaction_detail_id = transaction_detail_id;
		this.qty_product = qty_product;
		this.total = total;
		this.status = status;
		this.created_at = created_at;
		this.transaction = transaction;
		this.product = product;
	}
	
	private TransactionDetail() {}

	public int getTransaction_detail_id() {
		return transaction_detail_id;
	}

	public void setTransaction_detail_id(int transaction_detail_id) {
		this.transaction_detail_id = transaction_detail_id;
	}

	public int getQty_product() {
		return qty_product;
	}

	public void setQty_product(int qty_product) {
		this.qty_product = qty_product;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "TransactionDetail [transaction_detail_id=" + transaction_detail_id + ", qty_product=" + qty_product
				+ ", total=" + total + ", status=" + status + ", created_at=" + created_at + ", transaction="
				+ transaction + ", product=" + product + "]";
	}
}
