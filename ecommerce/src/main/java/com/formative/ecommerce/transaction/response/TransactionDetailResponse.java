package com.formative.ecommerce.transaction.response;

import java.util.List;

import com.formative.ecommerce.transaction.entity.TransactionDetail;

public class TransactionDetailResponse implements Response{
	int status;
	String message;
	List<TransactionDetail> data;
	
	public TransactionDetailResponse(int status, String message, List<TransactionDetail> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<TransactionDetail> getData() {
		return data;
	}

	public void setData(List<TransactionDetail> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TransactionDetailResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
	
	
}
