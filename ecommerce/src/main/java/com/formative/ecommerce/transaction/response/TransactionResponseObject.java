package com.formative.ecommerce.transaction.response;

import com.formative.ecommerce.transaction.entity.Transaction;

public class TransactionResponseObject implements Response{
	int status;
	String message;
	Transaction data;
	
	public TransactionResponseObject(int status, String message, Transaction data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Transaction getData() {
		return data;
	}

	public void setData(Transaction data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TransactionResponseObject [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
