package com.formative.ecommerce.transaction.response;

import java.util.List;

import com.formative.ecommerce.transaction.entity.Transaction;

public class TransactionResponse implements Response{
	int status;
	String message;
	List<Transaction> data;
	
	public TransactionResponse(int status, String message, List<Transaction> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<Transaction> getData() {
		return data;
	}
	
	public void setData(List<Transaction> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TransactionResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
