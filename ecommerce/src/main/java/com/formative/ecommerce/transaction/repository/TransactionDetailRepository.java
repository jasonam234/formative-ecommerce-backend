package com.formative.ecommerce.transaction.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.formative.ecommerce.transaction.entity.Transaction;
import com.formative.ecommerce.transaction.entity.TransactionDetail;
import com.formative.ecommerce.user.entity.User;

@Transactional
public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer>{
	@Query(value = "SELECT * FROM transaction_detail", nativeQuery = true)
	List<TransactionDetail> getAllTransactionDetail();
	
	@Query(value = "SELECT * FROM transaction_detail ORDER BY transaction_detail_id DESC LIMIT :limit", nativeQuery = true)
	List<TransactionDetail> getLastInsertedTransactionDetail(@Param("limit")int limit);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO transaction_detail(transaction_id, product_id, qty_product, total, status, created_at) "
			+ "VALUE(?1, ?2, ?3, ?4, false, now());"
			,nativeQuery = true)
	int transactionDetailCheckout(int transaction_id, int product_id, int qty_product, long total);
	
	@Modifying
	@Query(value = "UPDATE transaction_detail SET status = true WHERE transaction_id = ?1", nativeQuery = true)
	int putTransactionDetailStatus(int transaction_id);
}	
