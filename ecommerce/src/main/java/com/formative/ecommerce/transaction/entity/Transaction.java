package com.formative.ecommerce.transaction.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.formative.ecommerce.user.entity.User;

@Entity
@Table(name = "transaction")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int transaction_id;
	long grand_total;
	boolean status;
	String created_at;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName="user_id")
	User user;
	
	@OneToMany(mappedBy = "transaction")
	List<TransactionDetail> transactionDetail;
	
	public Transaction(int transaction_id, long grand_total, boolean status, String created_at, User user) {
		super();
		this.transaction_id = transaction_id;
		this.grand_total = grand_total;
		this.status = status;
		this.created_at = created_at;
		this.user = user;
	}

	private Transaction() {}

	public int getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(long grand_total) {
		this.grand_total = grand_total;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Transaction [transaction_id=" + transaction_id + ", grand_total=" + grand_total + ", status=" + status
				+ ", created_at=" + created_at + ", user=" + user + "]";
	}
}
