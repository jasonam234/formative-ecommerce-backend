package com.formative.ecommerce.store.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.formative.ecommerce.store.entity.Product;
import com.formative.ecommerce.user.entity.DetailUser;
import com.formative.ecommerce.user.entity.User;

@Transactional
public interface ProductRepository extends JpaRepository<Product, Integer>{
	@Query(value = "SELECT * FROM product WHERE store_id = ?1", nativeQuery = true)
	List<Product> getAllProduct(int store_id);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO product(store_id, product_name, price, description, image, created_at) "
			+ "VALUE(?1 ,?2 , ?3 , ?4, ?5, now());",
			nativeQuery = true)
	int insertProduct(int store_id, String product_name, long price, String description, String image);
}
