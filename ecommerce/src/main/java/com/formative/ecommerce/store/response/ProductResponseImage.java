package com.formative.ecommerce.store.response;

public class ProductResponseImage implements Response{
	int status;
	String message;
	
	public ProductResponseImage(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ProductResponseImage [status=" + status + ", message=" + message + "]";
	}
}
