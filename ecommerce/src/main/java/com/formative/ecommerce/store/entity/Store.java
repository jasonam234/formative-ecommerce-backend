package com.formative.ecommerce.store.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.formative.ecommerce.user.entity.User;

@Entity
@Table(name = "store")
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int store_id;
	String store_name;
	String city;
	boolean status;
	String created_at;	
	
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName="user_id")
	User user;
	
	@OneToMany(mappedBy = "store")
	List<Product> product;

	public Store(int store_id, String store_name, String city, boolean status, String created_at, User user) {
		super();
		this.store_id = store_id;
		this.store_name = store_name;
		this.city = city;
		this.status = status;
		this.created_at = created_at;
		this.user = user;
	}
	
	private Store() {}

	public int getStore_id() {
		return store_id;
	}

	public void setStore_id(int store_id) {
		this.store_id = store_id;
	}

	public String getStore_name() {
		return store_name;
	}

	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Store [store_id=" + store_id + ", store_name=" + store_name + ", city=" + city + ", status=" + status
				+ ", created_at=" + created_at + ", user=" + user + "]";
	}
}
