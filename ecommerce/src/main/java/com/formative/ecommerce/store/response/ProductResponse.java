package com.formative.ecommerce.store.response;

import java.util.List;

import com.formative.ecommerce.store.entity.Product;

public class ProductResponse implements Response{
	int status;
	String message;
	List<Product> data;
	
	public ProductResponse(int status, String message, List<Product> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Product> getData() {
		return data;
	}

	public void setData(List<Product> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ProductResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}

}
