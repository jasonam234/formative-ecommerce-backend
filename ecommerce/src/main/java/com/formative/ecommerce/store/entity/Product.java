package com.formative.ecommerce.store.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.formative.ecommerce.transaction.entity.TransactionDetail;

@Entity
@Table(name = "product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int product_id;
	String product_name;
	long price;
	String description;
	String image;
	String created_at;
	
	@ManyToOne
	@JoinColumn(name = "store_id", referencedColumnName="store_id")
	Store store;
	
	@OneToMany(mappedBy = "product")
	List<TransactionDetail> transactionDetail;

	public Product(int product_id, String product_name, long price, String description, String image, String created_at,
			Store store) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.price = price;
		this.description = description;
		this.image = image;
		this.created_at = created_at;
		this.store = store;
	}
	
	private Product() {}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	@Override
	public String toString() {
		return "Product [product_id=" + product_id + ", product_name=" + product_name + ", price=" + price
				+ ", description=" + description + ", image=" + image + ", created_at=" + created_at + ", store="
				+ store + "]";
	}
}
