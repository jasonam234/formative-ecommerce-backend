package com.formative.ecommerce.store.response;

import com.formative.ecommerce.store.entity.Product;

public class ProductResponseObject implements Response{
	int status;
	String message;
	Product data;
	
	public ProductResponseObject(int status, String message, Product data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Product getData() {
		return data;
	}

	public void setData(Product data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ProductResponseObject [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
	
	
}
