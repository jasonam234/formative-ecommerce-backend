package com.formative.ecommerce.store.response;

import java.util.List;

import com.formative.ecommerce.store.entity.Store;

public class StoreResponseObject implements Response{
	int status;
	String message;
	Store data;
	
	public StoreResponseObject(int status, String message, Store data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Store getData() {
		return data;
	}

	public void setData(Store data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "StoreResponseObject [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
