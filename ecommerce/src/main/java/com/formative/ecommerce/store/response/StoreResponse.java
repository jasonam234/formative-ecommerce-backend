package com.formative.ecommerce.store.response;

import java.util.List;

import com.formative.ecommerce.store.entity.Store;

public class StoreResponse implements Response{
	int status;
	String message;
	List<Store> data;
	
	public StoreResponse(int status, String message, List<Store> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Store> getData() {
		return data;
	}

	public void setData(List<Store> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "StoreResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
