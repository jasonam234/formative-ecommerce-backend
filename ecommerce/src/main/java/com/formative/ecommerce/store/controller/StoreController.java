package com.formative.ecommerce.store.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.formative.ecommerce.helper.FileUploadHelper;
import com.formative.ecommerce.store.entity.Product;
import com.formative.ecommerce.store.entity.Store;
import com.formative.ecommerce.store.repository.ProductRepository;
import com.formative.ecommerce.store.repository.StoreRepository;
import com.formative.ecommerce.store.response.ProductResponse;
import com.formative.ecommerce.store.response.ProductResponseImage;
import com.formative.ecommerce.store.response.ProductResponseObject;
import com.formative.ecommerce.store.response.Response;
import com.formative.ecommerce.store.response.StoreResponse;
import com.formative.ecommerce.store.response.StoreResponseError;
import com.formative.ecommerce.store.response.StoreResponseObject;

@RestController
public class StoreController {
	@Autowired 
	StoreRepository storeRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	//halaman dashboard user
	@GetMapping(value = "/api/stores", produces = "application/json")
	public ResponseEntity<Response> getAllStores() {
		List<Store> data = storeRepository.getAllStore();
		Response response = new StoreResponse(HttpStatus.OK.value(), "GET all stores operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	//post new store
	@PostMapping(value = "/api/store", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> postStore(@RequestBody Store store){
		storeRepository.insertStore(store.getUser().getUser_id(), store.getStore_name(), store.getCity());
		Store data = storeRepository.getLastStore();
		Response response = new StoreResponseObject(HttpStatus.OK.value(), "POST store operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	//halaman spesifik toko
	@GetMapping(value = "/api/products/{storeId}", produces = "application/json")
	public ResponseEntity<Response> getAllProduct(@PathVariable int storeId) {
		List<Product> data = productRepository.getAllProduct(storeId);
		Response response = new ProductResponse(HttpStatus.OK.value(), "GET all products operation succesful", data);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	//post product
	@PostMapping(value = "/api/product", consumes = "application/json")
	public ResponseEntity<Response> postProduct(@RequestBody Product product){		
		productRepository.insertProduct(product.getStore().getStore_id(), product.getProduct_name(), product.getPrice(), product.getDescription(), product.getImage());
		Response response = new ProductResponseObject(HttpStatus.OK.value(), "POST product operation succesful", product);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PutMapping(value = "/api/store/{id}", produces = "application/json")
	public ResponseEntity<Response> updateStoreStatus(@PathVariable int id) {
		Optional<Store> store = storeRepository.findById(id);
		if(store.isEmpty()) {
			StoreResponseObject response = new StoreResponseObject(HttpStatus.INTERNAL_SERVER_ERROR.value(), "INTERNAL_SERVER_ERROR",
					null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		store.get().setStatus(true);
		storeRepository.save(store.get());
		Response response = new StoreResponseObject(HttpStatus.OK.value(), "OK", store.get());
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping(value = "/api/store/true", produces = "application/json")
	public ResponseEntity<Response> getAllStoreTrue() {
		List<Store> stores = storeRepository.getAllStoreTrue();
//		if(stores.isEmpty()) {
//			Response response = new StoreResponseError(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
//					"INTERNAL_SERVER_ERROR");
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
//		}
		Response response = new StoreResponse(HttpStatus.OK.value(), "GET all store operation succesful", stores);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@GetMapping(value = "/api/store/false", produces = "application/json")
	public ResponseEntity<Response> getAllStoreFalse() {
		List<Store> stores = storeRepository.getAllStoreFalse();
//		if(stores.isEmpty()) {
//			StoreResponse response = new StoreResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "INTERNAL_SERVER_ERROR",
//					null);
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
//		}
		Response response = new StoreResponse(HttpStatus.OK.value(), "GET all user operation succesful", stores);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@GetMapping(value = "/api/store/{user_id}", produces = "application/json")
	public ResponseEntity<Response> getStoreByUserId(@PathVariable int user_id){
		Store store= storeRepository.getStoryByUserId(user_id);
		
		Response response = new StoreResponseObject(HttpStatus.OK.value(), "GET store operation succesful", store);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@PostMapping(value = "/api/product/upload_image")
	public ResponseEntity<Response> saveImage(@RequestParam("image") MultipartFile multipartFile) throws IOException{
		String filename = StringUtils.cleanPath(multipartFile.getOriginalFilename());

		String uploadDir = "src/main/resources/static/product_images/" + multipartFile.getOriginalFilename();
		FileUploadHelper.saveFile(uploadDir, filename, multipartFile);
		Response response = new ProductResponseImage(HttpStatus.OK.value(), "POST image operaionsuccesful");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
//	@GetMapping(value = "/api/product/image", produces = MediaType.IMAGE_JPEG_VALUE)
//	public void getImage(HttpServletResponse response) throws IOException {
//		var imgFile = new ClassPathResource("/src/main/resources/static/product_images/image_one.png/image_one.png");
//		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//        StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
//	}

	@GetMapping(value = "/api/product/image/{imageName}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<InputStreamResource> getImage(@PathVariable String imageName) throws IOException {

        var imgFile = new ClassPathResource("static/product_images/" + imageName + "/" + imageName);
        byte[] bytes = StreamUtils.copyToByteArray(imgFile.getInputStream());

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(new InputStreamResource(imgFile.getInputStream()));
    }
	
}
