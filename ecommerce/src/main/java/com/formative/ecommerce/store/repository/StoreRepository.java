package com.formative.ecommerce.store.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.formative.ecommerce.store.entity.Store;
import com.formative.ecommerce.user.entity.DetailUser;
import com.formative.ecommerce.user.entity.User;

@Transactional
public interface StoreRepository extends JpaRepository<Store, Integer>{
	@Query(value = "SELECT * FROM store", nativeQuery = true)
	List<Store> getAllStore();
	
	@Query(value = "SELECT * FROM store WHERE store_id=(SELECT LAST_INSERT_ID())", nativeQuery = true)
	Store getLastStore();
	
	@Query(value="SELECT * FROM store WHERE status = true", nativeQuery = true)
	List<Store> getAllStoreTrue();
	
	@Query(value="SELECT * FROM store WHERE status = false", nativeQuery = true)
	List<Store> getAllStoreFalse();
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO store(user_id, store_name, city, status, created_at) "
			+ "VALUE(?1, ?2, ?3, false, now());"
			,nativeQuery = true)
	int insertStore(int userId, String storeName, String city);

	@Query(value = "SELECT * FROM store WHERE user_id= ?1", nativeQuery = true)
	Store getStoryByUserId(int user_id);
}
