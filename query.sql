show databases;
drop database ecommerce;
create database ecommerce;
use ecommerce;

CREATE TABLE role(
	role_id int UNIQUE NOT NULL AUTO_INCREMENT,
    role_name varchar(50),
    created_at datetime,
    
    PRIMARY KEY(role_id)
);

CREATE TABLE detail_user(
	detail_user_id int UNIQUE NOT NULL AUTO_INCREMENT,
    fullname varchar(50),
    address varchar(50),
    phone_number varchar(50),
    created_at datetime,
    
    PRIMARY KEY(detail_user_id)
);

CREATE TABLE user(
	user_id int UNIQUE NOT NULL AUTO_INCREMENT,
    username varchar(50),
    password varchar(50),
    role int,
    detail_user int,
    created_at datetime,
    
    PRIMARY KEY(user_id),
    FOREIGN KEY(role) REFERENCES role(role_id) ON DELETE CASCADE,
	FOREIGN KEY(detail_user) REFERENCES detail_user(detail_user_id) ON DELETE CASCADE
);

CREATE TABLE store(
	store_id int UNIQUE NOT NULL AUTO_INCREMENT,
    user_id int,
    store_name varchar(50),
    city varchar(50),
    status boolean,
    created_at datetime,
    
    PRIMARY KEY(store_id),
    FOREIGN KEY(user_id) REFERENCES user(user_id) ON DELETE CASCADE
);

CREATE TABLE product(
	product_id int UNIQUE NOT NULL AUTO_INCREMENT,
    store_id int,
    product_name varchar(50),
    price long,
    description varchar(200),
	image varchar(200),
    created_at datetime,
    
    PRIMARY KEY(product_id),
    FOREIGN KEY(store_id) REFERENCES store(store_id) ON DELETE CASCADE
);

CREATE TABLE transaction(
	transaction_id int UNIQUE NOT NULL AUTO_INCREMENT,
    user_id int,
    grand_total long,
    status boolean,
    created_at datetime,
    
    PRIMARY KEY(transaction_id),
    FOREIGN KEY(user_id) REFERENCES user(user_id) ON DELETE CASCADE
);

CREATE TABLE transaction_detail(
	transaction_detail_id int UNIQUE NOT NULL AUTO_INCREMENT,
    transaction_id int,
    product_id int,
    qty_product int,
    total long,
    status boolean,
    created_at datetime,
    
    PRIMARY KEY(transaction_detail_id),
    FOREIGN KEY(transaction_id) REFERENCES transaction(transaction_id) ON DELETE CASCADE,
    FOREIGN KEY(product_id) REFERENCES product(product_id) ON DELETE CASCADE
);



INSERT INTO role(role_name, created_at) VALUE("User", now());
INSERT INTO role(role_name, created_at) VALUE("Owner", now());
INSERT INTO detail_user(fullname, address, phone_number, created_at) VALUE("AAA", "Address A", "00000", now());
INSERT INTO user(username, password, role, detail_user, created_at) VALUE ("aaa", "aaa@gmail.com", 1, 1, now());
INSERT INTO detail_user(fullname, address, phone_number, created_at) VALUE("BBB", "Address B", "11111", now());
INSERT INTO user(username, password, role, detail_user, created_at) VALUE ("bbb", "bbb@gmail.com", 2, 2, now());
INSERT INTO detail_user(fullname, address, phone_number, created_at) VALUE("CCC", "Address C", "33333", now());
INSERT INTO user(username, password, role, detail_user, created_at) VALUE ("ccc", "ccc@gmail.com", 2, 3, now());
INSERT INTO detail_user(fullname, address, phone_number, created_at) VALUE("DDD", "Address D", "44444", now());
INSERT INTO user(username, password, role, detail_user, created_at) VALUE ("ddd", "dddd@gmail.com", 2, 4, now());
INSERT INTO store(user_id, store_name, city, status, created_at) VALUE(2, "Store B", "City A", false, now());
INSERT INTO store(user_id, store_name, city, status, created_at) VALUE(3, "Store C", "City C", false, now());
INSERT INTO product(store_id, product_name, price, description, image, created_at) VALUE(1, "Product A", 500000, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "drive.com/image1.png", now());
INSERT INTO product(store_id, product_name, price, description, image, created_at) VALUE(1, "Product B", 150000, "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb", "drive.com/image2.png", now());
INSERT INTO product(store_id, product_name, price, description, image, created_at) VALUE(1, "Product C", 1000000, "ccccccccccccccccccccccccccccccccccccccccccccc", "drive.com/image3.png", now());
INSERT INTO Transaction(user_id, grand_total, status) VALUE (1,0,false);
INSERT INTO transaction_detail(transaction_id, product_id, qty_product, total, status) VALUE (1, 1, 2, 100000, false);

SELECT * FROM user;
SELECT * FROM ecommerce.store;
SELECT * FROM product;
SELECT * FROM Transaction;
SELECT * FROM transaction_detail;
SELECT * FROM ecommerce.store WHERE user_id=2;
DELETE FROM store WHERE store_id=4;